from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProjectForm

# Create your views here.


@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/list.html", context)


@login_required
def project_detail(request, id):
    project = get_object_or_404(Project, id=id)
    task = project.tasks.all()
    context = {
        "project": project,
        "task": task,
    }
    return render(request, "projects/detail.html", context)


def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()
    context = {"form": form}
    return render(request, "projects/create.html", context)
